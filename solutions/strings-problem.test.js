const { findFirstNonRepeatingCharacter } = require('./strings-problem');

const Tests = [
  ['hello', 'h'],
  ['beepBEEP', ''],
  ['BunNyBUN', 'y'],
  ['', '']
];

describe('findFirstNonRepeatingCharacter()', () => {
  for (const [testString, testOutput] of Tests) {
    it(`should return '${testOutput}' when input is '${testString}'`, () => {
      expect(findFirstNonRepeatingCharacter(testString)).toEqual(testOutput);
    });
  }
});
