function findDuplicateTransactions(txs = []) {
  return groupTxsByKey(txs)
    .map(txGroup => filterTxsWithSubsequentIntervals(txGroup, 60))
    .filter(txGroup => hasMoreThanOneTx(txGroup))
    .sort(sortGroupsByFirstTxTime)
}

function groupTxsByKey(txs = []) {
  const map = new Map();

  for (const tx of txs) {
    const key = buildKey(tx);
    if (!map.has(key)) {
      map.set(key, []);
    }
    map.get(key).push(tx);
  }

  return [...map.values()];
}

function filterTxsWithSubsequentIntervals(txs = [], txsInterval = 60) {
  const duplicateTxs = [];
  const sortedTxs = sortTxsByTime(txs);

  for (const [index, currentTx] of sortedTxs.entries()) {
    const hasDuplicateTxs = duplicateTxs.length > 0;

    if (hasDuplicateTxs) {
      const duplicateTx = lastItem(duplicateTxs);
      if (secondsBetweenTxs(currentTx, duplicateTx) <= txsInterval) {
        duplicateTxs.push(currentTx);
      }
    } else {
      const nextTx = sortedTxs[index + 1];
      if (secondsBetweenTxs(currentTx, nextTx) <= txsInterval) {
        duplicateTxs.push(currentTx);
      }
    }
  }

  return duplicateTxs;
}

function hasMoreThanOneTx(txs = []) {
  return txs.length > 1;
}

function sortGroupsByFirstTxTime(g1 = [], g2 = []) {
  const [firstTxG1] = g1;
  const [firstTxG2] = g2;

  return new Date(firstTxG1.time) - new Date(firstTxG2.time);
}

function buildKey(tx) {
  return `${tx.sourceAccount}:${tx.targetAccount}:${tx.amount}:${tx.category}`
}

function sortTxsByTime(txs = []) {
  return txs.sort((tx1, tx2) => new Date(tx1.time) - new Date(tx2.time))
}

function secondsBetweenTxs(tx1, tx2) {
  if (!tx1 || !tx2) {
    return NaN;
  }

  const seconds = (new Date(tx1.time) - new Date(tx2.time)) / 1000;
  return Math.abs(seconds);
}

function lastItem(array = []) {
  return array[array.length - 1];
}

module.exports = {
  findDuplicateTransactions,
}
