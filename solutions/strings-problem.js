function findFirstNonRepeatingCharacter(string = '') {
  const stringLC = string.toLowerCase();
  const checked = new Set();

  for (let idx = 0; idx < string.length; idx++) {
    const ch = stringLC.charAt(idx);

    if (!checked.has(ch) && stringLC.lastIndexOf(ch) === idx) {
      return string.charAt(idx);
    } else {
      checked.add(ch);
    }
  }

  return '';
}

module.exports = {
  findFirstNonRepeatingCharacter,
};
